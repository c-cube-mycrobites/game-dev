const assert =require("assert");
const games = require("../config/games.json");

describe("Unique Index of",()=>{
    it("All games",()=>{
        assert.strictEqual(games.every((game)=>game.index.length>0),true);
    })
    it("All levels of every game",()=>{
        assert.strictEqual(games.every((game)=>game.levels.every((level)=>level.index.length>0)),true);
    })
})