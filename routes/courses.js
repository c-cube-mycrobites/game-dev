const express = require("express"),
    courses = express.Router(),
    games = require("../config/games.json");

courses.get('/',(req,res)=>{
    res.redirect(`/courses/${games[0].index}/${games[0].levels[0].index}`)
})

courses.get('/:gameIndex/:levelIndex',(req,res,next)=>{
    res.setHeader("Content-Security-Policy", "script-src 'unsafe-eval' 'unsafe-inline' 'self'");
    const game = games.find((game)=>game.index==req.params.gameIndex);
    if(!game) next();
    else{ 
        const level = game.levels.find((level)=>level.index == req.params.levelIndex)
        level?res.render(`${game.index}.ejs`,{game,level}):res.redirect(`/courses/${game.index}/${game.levels[0].index}`);
    }
});

module.exports = courses;