
/**
 * For every new level. Default contructor repaints canvas, and resets everything.
*/
class Game1 {
  totalLevels = Number(getElement("totallevels").innerHTML);
  level = Number(getElement("thislevel").innerHTML)
  gameIndex = getElement("gameindex").innerHTML;

  allblocks = [{
    name:'down_move',
    color:220,
    caption:'Down',
    image:img.arrowDown,
    tip:"Moves one block downward",
    action:()=>`this.moveDown();`
  },{
    name:'forward_move',
    color:130,
    caption:'Forward',
    image:img.arrowRight,
    tip:"Moves one block forwards",
    action:()=>`this.moveForward();`
  },{
    name:'up_move',
    color:190,
    caption:'Up',
    image:img.arrowUp,
    tip:"Moves one block upwards",
    action:()=>`this.moveUp();`
  },{
    name:'backward_move',
    color:80,
    caption:'Back',
    image:img.arrowLeft,
    tip:"Moves one block backwards",
    action:()=>`this.moveBackward();`
  }]

  getGameBlocks(){
    switch(this.level){
      case 8:
      case 9:return [this.allblocks[0],this.allblocks[1],this.allblocks[2],this.allblocks[3]]
      default: return [this.allblocks[0],this.allblocks[1]];
    }
  }

  /**
   * Initializes blockly blocks.
   */
  initBlocks(){
    this.getGameBlocks().forEach((block)=>{
      Blockly.Blocks[block.name] = {
        init: function() {
          this.setColour(block.color);
          this.appendDummyInput()
            .appendField(block.caption)
            .appendField(new Blockly.FieldImage(
              block.image,
              15,
              15,
              "*"));
          this.setPreviousStatement(true);
          this.setNextStatement(true);
          this.setTooltip(block.tip)
        }
      };
      Blockly.JavaScript[block.name] = function(){
        return block.action();
      };
    })
  }


  canvas = getElement("canvas");
  context = this.canvas.getContext("2d");

  theobject = getElement("theobject");
  lostobject = getElement("lostobject");
  wonobject = getElement("wonobject");
  boxlayer = getElement("pathlayer")
  background = getElement("thebackground");

  spacing = 8;

  //box dimensions
  box = {
    width:24+this.spacing,
    height:24+this.spacing
  }

  //first box in matrix
  box00 = {
    width:this.box.width,
    height:this.box.height,
    startx:100-(this.spacing/2),
    starty:48-(this.spacing/2),
    endx:100-(this.spacing/2)+this.box.width,
    endy:48-(this.spacing/2)+this.box.height
  }

  totalboxes = 5*6;

  //box matrix object
  boxes = new Array(5).fill(0).map(() => new Array(6).fill(0)); //[y][x]

  object = {};

  //The coordinates of presence of boxes for each level
  boxcoordinates = [  //[x,y]
    [[4,3],[5,3],[4,4]],
    [[3,2],[4,2],[4,3],[5,3],[4,4]],
    [[2,2],[3,2],[4,2],[2,3],[4,3],[5,3]],
    [[2,1],[2,2],[3,2],[2,3],[3,3],[3,4],[4,4]],
    [[2,0],[3,0],[4,0],[1,1],[2,1],[4,1],[2,2],[3,2],[4,2],[4,3],[5,3],[4,4]],
    [[1,0],[0,1],[1,1],[4,1],[1,2],[2,2],[3,2],[4,2],[3,3],[4,3],[5,3],[4,4]],
    [[0,0],[3,0],[4,0],[0,1],[1,1],[2,1],[3,1],[4,1],[5,1],[1,2],[2,2],[3,2],[5,2],[3,3],[4,3],[5,3],[4,4]],
    [[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[0,1],[5,1],[0,2],[1,2],[2,2],[3,2],[5,2],[3,3],[5,3],[3,4],[4,4]],
    [[1,0],[2,0],[3,0],[0,1],[1,1],[3,1],[4,1],[0,2],[2,2],[4,2],[1,3],[3,3],[4,3],[1,4],[2,4]],
    [[3,0],[1,1],[2,1],[3,1],[4,1],[5,1],[0,2],[1,2],[3,2],[5,2],[1,3],[2,3],[3,3],[5,3],[3,4],[4,4]]
  ]

  //destination coordinate
  winCoordinate(pos){  //[x,y]
    switch(this.level){
      case 8: return [2,3][pos]
      case 9: return [4,2][pos]
      default:return [5,4][pos]
    }
  }

  getStartCoordinate(pos){
    const cpos =()=>{
      switch(this.level){
        case 8: return 3;
        case 9: return 6;
        default:return 0;
      }
    };
    return this.boxcoordinates[this.level][cpos()][pos];
  }

  /**
   * Checks if given coordinates are at any box coordinates.
   * @param {Number} x The x coordinate (w.r.t box coordinates)
   * @param {Number} y The y coordinate (w.r.t box coordinates)
   */
  isAtBoxCoordinate(x,y){
    return this.boxcoordinates[this.level].some((coord)=>{ return coord[0]==x&&coord[1]==y});
  }

  /**
   * Checks if given coordinates are at the destination coordinates.
   * @param {Number} x The x coordinate (w.r.t box coordinates)
   * @param {Number} y The y coordinate (w.r.t box coordinates)
   */
  isAtWinCoordinate(x,y){
    return x==this.winCoordinate(0)&&y==this.winCoordinate(1);
  }

  /**
   * Creates box matrix
   */
  initBoxes(){
    let y = 0;
    while(y<5){
      let x = 0;
      while(x<6){
        this.boxes[y][x] = this.isAtBoxCoordinate(x,y)?{
          width:this.box.width,
          height:this.box.height,
          startx:this.box00.startx+x*(this.box.width),
          endx:this.box00.endx+x*(this.box.width),
          starty:this.box00.starty+y*(this.box.height),
          endy:this.box00.endy+y*(this.box.height)
        }:0;
        x++;
      }
      y++;
    }
    console.log(this.boxes);
  }

  
  constructor() {
    this.initBlocks();
    this.initBoxes();
    this.workspace = new Workspace();
    this.enableRun();

    this.workspace.onClear(_=>{
      this.resetGame();
    });
    this.resetGame();
  }

  executeRun(){
    button.disable(this.workspace.run);
    let steps = this.workspace.getCode().split(';');
    steps[steps.length-1]?steps.push('this.enableRun()'):steps[steps.length-1]='this.enableRun()';
    console.log(steps);
    for(let i = steps.length-1;i>0;i--){//Adding steps in argument of their previous step.
      steps[i-1] = `${steps[i-1].substr(0,steps[i-1].length-2)}(_=>{${steps[i]}})`;
    }
    eval(steps[0]);
  }

  enableRun(){
    opacityOf(this.workspace.run,1);
    this.workspace.onRun(_=>{
      this.executeRun();
    }); 
  }
  /**
   * Resets the game canvas to default view.
   */
  resetGame(){
    this.clearCanvas();
    this.setBackground();
    this.setBoxLayer();
    this.resetObject();
    this.enableRun();
  }

  /**
   * Clears the canvas.
   */
  clearCanvas(){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height); 
  }

  /**
   * Sets background image on canvas
   */
  setBackground(){
    this.context.drawImage(this.background,0,0);
  }

  /**
   * 
   */
  setBoxLayer(){
    this.context.drawImage(this.boxlayer,0,0);
  }


  /**
   * Resets the object to default position.
   */
  resetObject(){
    const initBox = this.boxes[this.getStartCoordinate(1)][this.getStartCoordinate(0)];
    this.object = {
      width:this.theobject.width,
      height:this.theobject.height,
      startx:initBox.startx,
      endx:initBox.endx,
      starty:initBox.starty,
      endy:initBox.endy,
      x:this.getStartCoordinate(0),
      y:this.getStartCoordinate(1)
    }
    this.setObject();
  }

  /**
   * Sets the object to its current game position
   */
  setObject(){
    this.context.drawImage(this.theobject,this.object.startx,this.object.starty);
  }

  /**
   * Lose the object at its current game postion
   */
  loseObject(){
    this.context.drawImage(this.lostobject,this.object.startx,this.object.starty);
  }
  
  /**
   * Wins the object at its current game postion
   */
  winObject(){
    this.context.drawImage(this.wonobject,this.object.startx,this.object.starty);
  }

  afterWin(){
    if(this.level+1<this.totalLevels){
      window.location.href=`level${this.level+2}`
    } else {
      localStorage.removeItem(key.intro(this.gameIndex));
      new Dialog().showGameFinished();
    }
  }

  sudoMoveUp(afterMove=_=>{}){
    window.requestAnimationFrame(_=>{
      let yi = 0;
      let intv = setInterval(() => {
        this.object.starty-=2
        this.object.endy-=2
        this.setBoxLayer();
        this.setObject();
        yi+=2;
        if(yi>=this.box.width){
          clearInterval(intv);
          this.object.y-=1;
          afterMove();
        }
      },20)
      this.workspace.onClear(_=>{
        clearInterval(intv);
        this.resetGame();
      });
    })
  }

  /**
   * Moves the object forward without any checks
   */
  sudoMoveForward(afterMove=_=>{}){
    window.requestAnimationFrame(_=>{
      let xi = 0;
      let intv = setInterval(() => {
        this.object.startx+=2
        this.object.endx+=2
        this.setBoxLayer();
        this.setObject();
        xi+=2;
        if(xi>=this.box.width){
          clearInterval(intv);
          this.object.x+=1;
          afterMove();
        }
      },20)
      this.workspace.onClear(_=>{
        clearInterval(intv);
        this.resetGame();
      });
    })
  }

  /**
   * Moves the object downwards without any checks
   */
  sudoMoveDown(afterMove=_=>{}){
    window.requestAnimationFrame(_=>{
      let yi = 0;
      let intv = setInterval(() => {
        this.object.starty+=2;
        this.object.endy+=2;
        this.setBoxLayer();
        this.setObject();
        yi+=2;
        if(yi>=this.box.height){
          clearInterval(intv);
          this.object.y+=1;
          afterMove();
        }
      }, 20);
      this.workspace.onClear(_=>{
        clearInterval(intv);
        this.resetGame();
      });
    })
  }

  sudoMoveBack(afterMove=_=>{}){
    window.requestAnimationFrame(_=>{
      let xi = 0;
      let intv = setInterval(() => {
        this.object.startx-=2
        this.object.endx-=2
        this.setBoxLayer();
        this.setObject();
        xi+=2;
        if(xi>=this.box.width){
          clearInterval(intv);
          this.object.x-=1;
          afterMove();
        }
      },20)
      this.workspace.onClear(_=>{
        clearInterval(intv);
        this.resetGame();
      });
    })
  }

  moveUp(onMoved){
    const newcoords = [this.object.x,this.object.y-1]
    const dial = new Dialog();
    this.sudoMoveUp(
      !this.isAtBoxCoordinate(
        newcoords[0],newcoords[1]
      )?_=>{
        if(!this.isAtWinCoordinate(newcoords[0],newcoords[1])){
          this.loseObject()
          dial.showLose(_=>{},_=>{
            this.resetGame();
            dial.hide();
          },img.game(this.gameIndex).objectlost);
        } else {
          onMoved == "_=>{this.enableRun()}"
            ?dial.showWin(_=>{
              this.winObject();
              this.afterWin()
            },img.game(this.gameIndex).objectwon)
            :onMoved();
        }
      }:_=>{
        onMoved();
      });   
  }

  /**
   * Moves object in forwards, with checking invalid coordinates and showing alerts.
   * @param {Function} onMoved The method to be executed after succesfull movement. Will not execute if error occurs.
   */
  moveForward(onMoved){
    const newcoords = [this.object.x+1,this.object.y]
    const dial = new Dialog();
    this.sudoMoveForward(
      !this.isAtBoxCoordinate(
        newcoords[0],newcoords[1]
      )?_=>{
        if(!this.isAtWinCoordinate(newcoords[0],newcoords[1])){
          this.loseObject()
          dial.showLose(_=>{},_=>{
            this.resetGame();
            dial.hide();
          },img.game(this.gameIndex).objectlost);
        } else {
          onMoved == "_=>{this.enableRun()}"
            ?dial.showWin(_=>{
              this.winObject();
              if(this.level+1<this.totalLevels){
                window.location.href=`level${this.level+2}`
              } else {
                localStorage.removeItem(key.intro(this.gameIndex));
                dial.showGameFinished();
              }
            },img.game(this.gameIndex).objectwon)
            :onMoved();
        }
      }:_=>{
        onMoved();
      });  
  }

  /**
   * Moves object in downwards, with checking invalid coordinates and showing alerts.
   * @param {Function} onMoved The method to be executed after succesfull movement. Will not execute if error occurs.
   */
  moveDown(onMoved){
    const newcoords = [this.object.x,this.object.y+1]
    console.log(newcoords);
    console.log(this.isAtBoxCoordinate(
      newcoords[0],newcoords[1]
    ))
    const dial = new Dialog();
    this.sudoMoveDown(
      !this.isAtBoxCoordinate(
        newcoords[0],newcoords[1]
      )?_=>{
        if(!this.isAtWinCoordinate(newcoords[0],newcoords[1])){
          this.loseObject();
          dial.showLose(_=>{},_=>{
            this.resetGame();
            new Dialog().hide();
          },img.game(this.gameIndex).objectlost);
        } else {
          onMoved == "_=>{this.enableRun()}"
            ?dial.showWin(_=>{
              this.winObject();
              if(this.level+1<this.totalLevels){
                window.location.href=`level${this.level+2}`
              } else {
                dial.showGameFinished();
              }
            },img.game(this.gameIndex).objectwon)
            :onMoved();
        }
      }:_=>{
        onMoved();
      }
    );
  }


  moveBackward(onMoved){
    const newcoords = [this.object.x-1,this.object.y]
    const dial = new Dialog();
    this.sudoMoveBack(
      !this.isAtBoxCoordinate(
        newcoords[0],newcoords[1]
      )?_=>{
        if(!this.isAtWinCoordinate(newcoords[0],newcoords[1])){
          this.loseObject()
          dial.showLose(_=>{},_=>{
            this.resetGame();
            dial.hide();
          },img.game(this.gameIndex).objectlost);
        } else {
          onMoved == "_=>{this.enableRun()}"
            ?dial.showWin(_=>{
              this.winObject();
              this.afterWin()
            },img.game(this.gameIndex).objectwon)
            :onMoved();
        }
      }:_=>{
        onMoved();
      });   
  }

  /**
   * @deprecated
   * Saves and displays score, updates if needed.
   * @param {Boolean} update If true, will increase the score by 1 and save.
   */
  setScore(update = false){
    const id = `${this.level}score`
    let score;
    if(update){
      score = localStorage.getItem(id)?Number(localStorage.getItem(id))+1:1
    } else {
      score = localStorage.getItem(id)||0;
    }
    localStorage.setItem(id,score);
    getElement("score").innerHTML = localStorage.getItem(id);
    getElement("clearScore").onclick =_=>{ 
      localStorage.removeItem(id)
      getElement("score").innerHTML = 0;
    }
  }
}

window.onload = (_) =>{
  localStorage
    .getItem(key.intro(getElement('gameindex').innerHTML))
      ?new Game1()
      :new Dialog().showIntro(_=>{
        new Game1();
        localStorage.setItem(key.intro(getElement('gameindex').innerHTML),false)
      },getElement('gamename').innerHTML,getElement('gamedescription').innerHTML,img.game(getElement('gameindex').innerHTML,getElement('thislevel').innerHTML).object)
};
