/**
 * Keys for general purposes
 */
class Keys{
  level = "level";
  code = "code";
  intro =(gameindex) =>`${gameindex}intro`;
  score = "score";
  emptymethod = "_=>{}";
  winheadings = ['Great! You won.'];
  winbody = [`You've done a great job.`];
  loseheadings = ['Umm...Nevermind'];
  losebody = [`Just a little more practice, you're almost there.`];
  defaultHints = ['Try this one yourself.']
}

/**
 * Images sources
 */
class Images{
  folder = '/images'
  arrowDown = `${this.folder}/arrow_down.png`;
  arrowRight = `${this.folder}/arrow_right.jpg`;
  arrowUp = `${this.folder}/arrow_up.png`;
  arrowLeft = `${this.folder}/arrow_left.png`;
  congrats = `${this.folder}/congrats.svg`;
  /**
   * Returns images for games as per params.
   * @param {String} gameindex The game config index
   * @param {Number} level The level numeric index
   * @return {JSON} object and background
   */
  game(gameindex,level){
    return {
      object:`${this.folder}/${gameindex}/object.svg`,
      objectlost:`${this.folder}/${gameindex}/objectlost.svg`,
      objectwon:`${this.folder}/${gameindex}/objectwon.svg`,
      background:`${this.folder}/${gameindex}/background.svg`,
      background:`${this.folder}/${gameindex}/${level}pathlayer.svg`
    }
  }
}

/**
 * Server paths
 */
class Locations{
  root = "/";
  courses = "/courses";
}

class Buttons{
  primary =(id,value)=> this.getButton('primary-button',id,value)
  passive =(id,value)=> this.getButton('passive-button',id,value)
  blue = (id,value)=> this.getButton('blue-button',id,value)
  red = (id,value)=> this.getButton('danger-button',id,value)
  getButton=(csstype,id,value)=> `<button class="${csstype}" id="${String(id).toLowerCase()}">${value||id}</button>`;
  disable(button){button.onclick=_=>{}; opacityOf(button,0.5)}
}

/**
 * Initializes default workspace for every level (possibly every game)
 */
class Workspace{
  showlevels = getElement("showlevels");
  run = getElement("runcode");
  undo = getElement("undo");
  redo = getElement("redo");
  clear = getElement("clear");
  showcode = getElement("showcode");
  constructor(){
    this.clear.onclick = (_) => {
      Blockly.mainWorkspace.clear();
    };
    this.undo.onclick = (_) => {
      Blockly.mainWorkspace.undo();
    };
    this.redo.onclick = (_) => {
      Blockly.mainWorkspace.undo(true);
    };
    this.showlevels.onclick=_=>{
      new Dialog().showAllLevels();
    }
    this.showcode.onclick = (_) => {
      new Dialog().showCode(this.getCode())
    };

    this.workspace = Blockly.inject("blocklyDiv", {
      toolbox: document.getElementById("toolbox"),
      toolboxPosition : 'end', 
      scrollbars : false,
      trashcan : false, 
      sounds : false, 
      zoom:
        {controls: false,
          wheel: false,
          startScale: 1.3,
          maxScale: 4,
          minScale: 0.5,
          scaleSpeed: 1.5,
          pinch: false
        },
      grid:{
        spacing: 40,
        length: 6,
        colour: '#ccc',
        snap: true
      }
    });
  }

  /**
   * @returns {String} Generated workspace code
   */
  getCode(){
    Blockly.JavaScript.addReservedWords(key.code);
    return Blockly.JavaScript.workspaceToCode(this.workspace);
  }

  /**
   * Executes method on run click
   * @param {Function} action Method on run action
   */
  onRun(action=_=>{eval(this.getCode())}){
    this.run.onclick=_=>{
      action();
    }
  }

  /**
   * Executes method on clear click, after clearing workspace
   * @param {Function} action Method on clear action
   */
  onClear(action=_=>{}){
    this.clear.onclick=_=>{
      Blockly.mainWorkspace.clear();
      action();
    }
  }

  /**
   * Executes method on undo click, after undoing workspace.
   * @param {Function} action Method on undo action
   */
  onUndo(action=_=>{}){
    this.undo.onclick = (_) => {
      Blockly.mainWorkspace.undo();
      action()
    };
  }

  /**
   * Executes method on redo click, after redoing workspace
   * @param {Function} action Method on redo action
   */
  onRedo(action=_=>{}){
    this.redo.onclick = (_) => {
      Blockly.mainWorkspace.undo(true);
      action()
    };
  }

  /**
   * Executes method on show code click, after showing code on dialog
   * @param {Function} action Method on show code action
   */
  onShowCode(action=_=>{}){
    this.showcode.onclick = (_) => {
      new Dialog().showCode(this.getCode())
      action();
    };
  }
}

/**
 * Sets the opacity of given HTML element.
 * @param {HTMLElement} element The element whose opacity is to be set.
 * @param {Number} value The numeric value of opacity of the given element to set. Defaults to 1. Must be >=0 & <=1.
 */
const opacityOf = (element = new HTMLElement(), value = 1) =>
  (element.style.opacity = String(value));

/**
 * Sets the opacity of HTML element(s) of given array.
 * @param {Array} elements The array of whose element(s)'s opacity is to be set.
 * @param {Number} value The numeric value of opacity of the element(s) to set. Defaults to 1. Must be >=0 & <=1.
 * @param {Number} index The particular index element's opacity to be set. If not provided, will set the opacity for all elements.
 */
const opacityOfAll = (elements = [], value = 1,index = null) => index!=null? opacityOf(elements[index],value):elements.forEach((element)=>opacityOf(element,value))

/**
 * Controls visiblity of the given HTML element, as per the condition. (using hidden attribute of HTMLElement)
 * @param {HTMLElement} element The element whose visibility is to be toggled.
 * @param {Boolean} visible The boolean value to show or hide the given element. Defaults to true (shown).
 */
const visibilityOf = (element, visible = true) =>{
  if(!element) return;
  element.hidden = !visible;
  element.style.display = visible?"block":"none";
}

/**
 * Controls visiblity of the HTML elements in given array, as per the condition. (using hidden attribute of HTMLElement).
 * @param {Array} elements The array of elements whose element's visibility is to be toggled.
 * @param {Boolean} visible The boolean value to show or hide the given element. Defaults to true (shown). The visibility of rest elements will be toggled oppositly.
 * @param {Number} index If not set, all the elements will have the same visibility state, else the element at given index will have the provided visibility state, and others opposite.
 */
const visibilityOfAll = (elements = [], visible = true, index = null) =>{
  index != null
    ?elements.forEach((element, e) => {
      visibilityOf(element, e==index);
    })
    : elements.forEach((element, _) => {
      visibilityOf(element, visible);
    });
}

/**
 * @param {Array} array A non empty array
 */
const getRandomValue=(array = [''])=> array[getRandomInt(array.length)];

/**
 * 
 * @param {Number} max Integral Limit of randomness
 * @returns {Number} A random number between 0 and max
 */
const getRandomInt=(max)=>Math.floor(Math.random() * Math.floor(max));

/**
 * 
 * @param {String} id Element id
 * @returns {HTMLElement} by id
 */
const getElement =(id)=> document.getElementById(id);

const locate = new Locations();
const key = new Keys();
const button = new Buttons();
const img = new Images();
