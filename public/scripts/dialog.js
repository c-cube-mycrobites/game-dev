
/**
 * Initializes new dialog every time in constructor.
 */
class Dialog{
    headers = ['dialog-header-blue','dialog-header-yellow','dialog-header-red','dialog-header-green'];
    dialog = getElement("dialog");
    constructor(){
        this.dialog.innerHTML = `
        <div class="dialog-content">
          <div class="${this.headers[0]}" id="dialogheader">
            <span id="dialogheading"></span>
          </div>
          <br/>
          <div class="w3-row" id="dialogimg">
          </div>
          <div class="dialog-body" id="dialogbody">
          </div>
          <div class="dialog-footer" id="dialogactions">
          </div>
        </div>`;
        this.heading = getElement('dialogheading');
        this.img = getElement('dialogimg');
        this.body = getElement('dialogbody');
        this.actions = getElement('dialogactions');
    }

    /**
     * Introduction dialog
     * @param {Function} onstart Method on start click
     * @param {String} heading Heading
     * @param {String} body body
     * @param {String} imgsrc optional image
     */
    showIntro(onstart=_=>{},heading = 'Welcome',body,imgsrc){
        this.hide();
        this.blueHead();
        this.setHeading(heading);
        this.setImage(imgsrc);
        this.setBody(body);
        this.setActions([button.blue('startplay','Start Playing')],[_=>{onstart();this.hide()}]);
        this.show();
    }

    setHideable(hideonclick = true){
        window.onclick=(event)=>{
            if(hideonclick&&event.target == this.dialog){
                this.hide();
            }
        }
    }
    showAllLevels(){
        this.hide();
        this.yellowHead();
        this.setHeading('Levels');
        this.setBody('...');
        this.show();
    }

    /**
     * Shows code in dialog
     * @param {String} code The code to be displayed
     */
    showCode(code){
        this.hide();
        this.blueHead();
        this.setHeading('Your code');
        code = code.split('this.').join('<br/>');
        this.setBody(code?`<code>${code}</code>`:'Place your first block to proceed.');
        this.setActions([button.primary('Resume')],[_=>this.hide()])
        this.show();
    }

    /**
     * Shows hint dialog
     * @param {String} hint The hint text
     * @param {String} imgsrc optional image source
     */
    showHint(hint,imgsrc){
        this.hide();
        this.yellowHead();
        this.setHeading('Hint');
        this.setImage(imgsrc);
        this.setBody(hint||getRandomValue(key.defaultHints));
        this.setActions([button.primary('Resume')],[_=>this.hide()]);
        this.show();
    }

    /**
     * Shows winning dialog.
     * @param {Function} nextAction Method to be excecuted after next is clicked
     * @param {String} imgsrc optional image source
     */
    showWin(nextAction=_=>{},imgsrc){
        this.hide();
        this.greenHead();
        this.setHeading(getRandomValue(key.winheadings));
        this.setImage(imgsrc);
        this.setBody(key.winbody);
        this.setActions([button.blue('nextlevel','Next level'),button.passive('Replay')],[_=>{nextAction()},_=>{window.location.reload()}])
        this.show();
    }

    /**
     * Shows game lost dialog
     * @param {Function} learnaction Method on learn action
     * @param {Function} retryAction Method on retry action
     * @param {String} imgsrc optional image source
     */
    showLose(learnaction=_=>{},retryAction=_=>{window.location.reload()},imgsrc){
        this.hide();
        this.redHead();
        this.setHeading(getRandomValue(key.loseheadings));
        this.setImage(imgsrc);
        this.setBody(getRandomValue(key.losebody));
        this.setActions([button.primary('practise','Watch video'),button.blue('Retry')],[
            _=>{learnaction()}
            ,_=>{retryAction()}
        ]);
        this.show();
    }
    
    /**
     * Shows game completion dialog
     * @param {String} imgsrc optional image source
     */
    showGameFinished(imgsrc = img.congrats){
        this.hide();
        this.blueHead();
        this.setHeading('Congratulations!')
        this.setImage(imgsrc);
        this.setBody(`You've successfully completed this game course.`);
        this.setActions([button.blue('browsecourses','Browse other courses'),button.passive('Share')],[_=>{window.location.href=locate.root}])
        this.show();
    }
    blueHead(){
        this.headers.forEach((header)=>{
            getElement('dialogheader').classList.replace(header,'dialog-header-blue');
        });
    }
    redHead(){
        this.headers.forEach((header)=>{
            getElement('dialogheader').classList.replace(header,'dialog-header-red');
        });
    }
    yellowHead(){
        this.headers.forEach((header)=>{
            getElement('dialogheader').classList.replace(header,'dialog-header-yellow');
        });
    }
    greenHead(){
        this.headers.forEach((header)=>{
            getElement('dialogheader').classList.replace(header,'dialog-header-green');
        });
    }
    setHeading(head){
        this.heading.innerHTML = head;
    }
    setBody(body){
        this.body.innerHTML = body
    }
    setImage(imgsrc){
        this.img.innerHTML = imgsrc?`<img src="${imgsrc}" width="260" />`:'';
    }
    buttons = [];
    setActions(buttons = [],onclicks = []){
        this.actions.innerHTML = '';
        buttons.forEach((button)=>{
            this.actions.innerHTML+=button
        });

        buttons.forEach((button)=>{
            const temp = button.split("\"");
            this.buttons.push(getElement(temp[temp.length-2].toLowerCase()));
        });
        this.setButtonOnclicks(onclicks);
    }
    setButtonOnclicks(actions = []){
        actions.forEach((action,a)=>{
            this.buttons[a].onclick=_=>{
                action();
            }
        })
    }
    /**
     * Hides dialog
     */
    hide(){
        visibilityOf(this.dialog,false);
    }
    /**
     * Shows dialog
     */
    show(){
        if(!this.buttons.length){
            this.setHideable(true);
        } else {
            this.setHideable(false);
        }
        visibilityOf(this.dialog,true);
    }
// When the user clicks anywhere outside of the dialog, close it
}