# Game-Dev

## Configuration

* Game-dev games
* 0.0.1
* Built over NodeJS v15.4.0, npm v6.14.0

## Developement

Clone the repository, then run following commands.

```bash
npm install
npm run dev
```

The above commands log

```bash
Started: http://localhost:3000
```

Navigate to the given host:port on your browser.

## Contribution

To test

```bash
npm test
```

On changes done,

```bash
npm run commit
```

The above command will initiate commiting step by step to maintain commit pattern.
After successfull commit,

```bash
git push -u origin master
```

## More help

Contact

* Repo owner or admin
* Other community or team contact
