const express = require("express"),
    server = express(),
    helmet = require("helmet");

server.set("view engine", "ejs");
server.use(helmet());
server.use(express.static("public"));


server.get('/',(req,res)=>{
    res.redirect(`courses`);
});

server.use('/courses',require('./routes/courses'));

server.get("/404", (__, _, next) => {
    next();
});

server.get("/403", (__, _, next) => {
    next();
});

server.get("/500", (__, _, next) => {
    next();
});

server.use((req, res, _) => {
    res.status(404);
    res.format({
        html: ()=> {
        res.render("404.ejs", { url: req.url });
        },
        json: ()=> {
        res.json({ error: "Not found" });
        },
        default: ()=>{
        res.type("txt").send("Not found");
        },
    });
});

const server_port = process.env.PORT|| 3000 || 80;
const server_host = '0.0.0.0' || 'localhost';

server.listen(server_port,server_host,()=>{
    console.log(`Started: http://${server_host}:${server_port}`);
})